"""
Set of commonly used pandas dataframe processing functions.
"""


def categorize(data_frame, column_name, prefix="", suffix=""):
    """
    Catrgorizes a column_name.

    Say if a column named letter consis of three unique avlues A, B C. This
    function adds three columns A, B, C to data frame with True or False
    values to it.

    Parameters
    ----------
    data_frame : DataFrame
        The data frame who's column must be categorized.
    column_name : str
        Name of the column that must be categorized.

    Returns
    -------
    None

    """

    unique_values = data_frame[column_name].unique()

    for unique_value in unique_values:
        if data_frame[column_name].dtype == object:
            filter_unique = data_frame[column_name].str.contains(unique_value)
        else:
            filter_unique = data_frame[column_name] == unique_value
        data_frame[prefix + str(unique_value) + suffix] = filter_unique

def type_convert(data_frame, column_names, new_type):
    """
    Converts column type from one to another.

    Parameters
    ----------
    data_frame : DataFrame
        The data frame who's column must be type converted.
    column_name : str
        Name of the column that must be type converted.
    new_type: type

    Returns
    -------
    None

    """

    for column_name in column_names:
        data_frame[column_name] = data_frame[column_name].astype(new_type)

def cols_delete(data_frame, column_names):
    """
    Deletes columns.

    Parameters
    ----------
    data_frame : DataFrame
        Data frame who's columns must be deleted
    column_names : list
        list of column names

    Returns
    -------
    None

    """

    for column_name in column_names:
        del data_frame[column_name]

def cols_search(data_frame, string):
    """
    Serches for column names that contains the string.


    Parameters
    ----------
    data_frame : DataFrame
        Data frame which must be searched.
    string : str
        String must be present in the column name.

    Returns
    -------
    list
        column names

    """

    columns = [column_name for column_name in data_frame.columns if string in column_name]
    return columns

def col_contains(data_frame, column_name, strings):
    """
    Returns the filtered data frame with rows that contain strin in column_name

    Parameters
    ----------
    data_frame : DataFrame
        The data frame which must be searched.
    column_name : str
        Name of the column that must contain the string

    Returns
    -------
    DataFrame
        Filtered Data Frame that contains sting in given column_name

    """

    constructed_filter = data_frame[column_name].str.contains(strings[0])

    for string in strings:
        constructed_filter = constructed_filter | data_frame[column_name].str.contains(string)

    return data_frame[constructed_filter]
